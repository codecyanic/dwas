# Desktop WebKit Application Sandbox

Run a web application as a desktop application.

## Features

With DWAS you can:

- Apply rules to allow and block loading of pages and resources
- Keep cookies and local storage while not leaking identity to other sites
- Optionally remove all data when application is closed
- Inject custom scripts to enhance functionality
- Apply user styles

## Building

In order to enable resource filtering, you need to build a binary component.
For this you will need gcc, pkg-config, setuptools and development files for Python, PyGObject and WebKitGtk.

On Debian/Ubuntu run:
```
apt install gcc python3-dev python3-setuptools python-gi-dev libwebkit2gtk-4.0-dev
```

You can build and install the dwas module for the current user with:
```
./setup.py install --user
```

## Running

To run the module you need to have PyGObject, WebKitGtk and PyYAML installed.
Also `pkg_resources` from setuptools is required for using installed `dwas` script.

On Debian/Ubuntu install runtime requirements with:
```
apt install python3-gi gir1.2-webkit2-4.0 python3-yaml python3-pkg-resources
```

To test if everything works, you can run:
```
dwas example.yaml
```
or
```
python3 -m dwas example.yaml
```

## Usage

All configuration is stored in manifest files.
To run a manifest file, provide it's path as a command line parameter to the `dwas` command.

### App directories

App directories are directories containing a manifest file named `manifest.yaml`.
They can be run by providing the directory path (instead of the manifest path) as a command line parameter to DWAS.
When run this way, DWAS will store all data in the provided directory.

### Manifest files

A manifest file is a YAML file containing all the information needed to run the application.

The only strictly required field is `url`.
It specifies which page to load first.

In case a manifest file is run directly, `name` field is also required.
It is used to distinguish between applications, even if their `url` fields are the same.
In this case application data is stored in `~/.dwas/NAME/`, where `NAME` is derived from the `name` field.

There are two methods for a page or a resource to be blocked.
The `allow` field is used for whitelisting and `deny` for blacklisting.
Their values are arrays of regex strings which have to match full URIs.
Loading will occur only if there are matches for `allow` rules, and no matches for `deny` rules.
If `allow` is omitted, only URIs starting with string specified in the `url` field will load.

When `ephemeral` is set to `true`, data will be deleted when application window is closed.
Value of `javascript` field will be executed on page load and `style` value will be set as a CSS user style.

For a working example, check out [example.yaml](example.yaml).

## Final notes

The software uses WebKitGtk and as such may behave differently depending on WebKitGtk version.

It is still under development and you may expect breaking changes between releases.
Some features might not work properly, others might not be documented.
