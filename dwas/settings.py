# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class WebSettings:
    defaults = {
        'enable-java': False,
        'enable-webaudio': False,
        'enable-webgl': False,
    }

    enabled = [
        'enable-developer-extras',
        'enable-fullscreen',
        'enable-html5-database',
        'enable-html5-local-storage',
        'enable-javascript',
        'enable-javascript-markup',
        'enable-media',
        'enable-offline-web-application-cache',
        'enable-smooth-scrolling',
        'enable-tabs-to-links',
        'enable-webaudio',
        'enable-webgl',
        'enable-write-console-messages-to-stdout',
        'media-playback-requires-user-gesture',
        'user-agent',
    ]
