# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import webbrowser

from urllib.parse import quote
from shutil import rmtree
from .settings import WebSettings

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')

from gi.repository import Gtk, Gdk, GLib, WebKit2  # noqa E402


class DWAS:
    def __init__(self, manifest, basedir=None):
        self.manifest = manifest
        self.window = self.new_window()

        self.max_icon_width = 0

        if basedir:
            self.basedir = basedir
        else:
            self.basedir = self.generate_basedir(self.manifest['name'])

        self.webview = self.new_webview()

        self.set_user_style()

        s = self.webview.get_settings()
        for setting in WebSettings.defaults.keys():
            s.set_property(setting, WebSettings.defaults[setting])

        settings = self.manifest.get('settings', {})
        for setting in settings.keys():
            if setting in WebSettings.enabled:
                s.set_property(setting, settings[setting])

        sw = Gtk.ScrolledWindow()
        self.window.add(sw)
        sw.add(self.webview)

        self.gtk_connect()
        self.webview.load_uri(self.manifest['url'])
        GLib.timeout_add_seconds(1, self.window.show_all)

    def __del__(self):
        if not hasattr(self, 'basedir'):
            return

        if hasattr(self, 'lasticon'):
            path = os.path.join(self.basedir, 'favicon.png')
            try:
                self.lasticon.write_to_png(path)
            except Exception as e:
                print(e)

        if hasattr(self, 'manifest'):
            if self.manifest.get('ephemeral'):
                try:
                    rmtree(self.icons_path)
                except FileNotFoundError:
                    pass
                except AttributeError:
                    pass

                try:
                    os.rmdir(self.basedir)
                except OSError:
                    pass

    def new_window(self):
        width = 1280
        try:
            w = self.manifest['window']['width']
            if w > 0:
                width = w
        except Exception:
            pass

        height = 640
        try:
            h = self.manifest['window']['height']
            if h > 0:
                height = h
        except Exception:
            pass

        window = Gtk.Window()
        window.set_default_size(width, height)
        window.set_title('Desktop Webkit Application Sandbox')
        return window

    def gtk_connect(self):
        self.webview.connect('create', self.on_create)
        self.webview.connect('decide-policy', self.on_decide_policy)
        self.webview.connect('notify::title', self.set_title)
        self.webview.connect('notify::favicon', self.set_icon)
        self.webview.connect('load-changed', self.on_load_changed)

    def new_webview(self):
        path = self.basedir
        if self.manifest.get('ephemeral'):
            context = WebKit2.WebContext.new_ephemeral()
        else:
            dm = WebKit2.WebsiteDataManager(base_data_directory=path,
                                            base_cache_directory=path)
            context = WebKit2.WebContext.new_with_website_data_manager(dm)

        self.icons_path = os.path.join(path, 'icons')
        context.set_favicon_database_directory(self.icons_path)

        extdir = os.path.abspath(os.path.dirname(__file__))
        context.set_web_extensions_directory(extdir)

        user_data = GLib.Variant.new_string(str(self.manifest))
        context.set_web_extensions_initialization_user_data(user_data)

        return WebKit2.WebView.new_with_context(context)

    def generate_basedir(self, name):
        if not name:
            raise ValueError("name is empty")
        home = os.path.expanduser('~')
        safename = quote(name, safe=' ')
        if safename in ['.', '..']:
            safename = safename.replace('.', '%2E')
        return os.path.join(home, '.dwas', safename)

    def on_create(self, view, action):
        uri = action.get_request().get_uri()
        self.open_external(uri)

    def open_external(self, uri):
        if self.manifest.get('use-external'):
            print('Opening:', uri)
            webbrowser.open(uri)
        else:
            print('Ignoring new window:', uri)

    def set_user_style(self):
        style = self.manifest.get('style')
        if not style:
            return
        cm = self.webview.get_user_content_manager()
        frames = WebKit2.UserContentInjectedFrames.ALL_FRAMES
        level = WebKit2.UserStyleLevel.USER
        style_sheet = WebKit2.UserStyleSheet(str(style), frames, level)
        cm.add_style_sheet(style_sheet)

    def on_load_changed(self, view, event, data=None):
        if event == WebKit2.LoadEvent.FINISHED:
            js = self.manifest.get('javascript')
            if js:
                self.webview.run_javascript(js)
            self.window.show_all()

    def set_icon(self, view, data=None):
        icon = view.get_favicon()
        width = icon.get_width()
        if width >= self.max_icon_width:
            self.max_icon_width = width
            height = icon.get_height()
            pixbuf = Gdk.pixbuf_get_from_surface(icon, 0, 0, width, height)
            self.window.set_icon(pixbuf)
            self.lasticon = icon

    def set_title(self, view, gparam):
        self.window.set_title(view.get_title())

    def on_decide_policy(self, widget, decision, dtype):
        types = WebKit2.PolicyDecisionType

        uri = decision.get_request().get_uri()
        if dtype == types.NAVIGATION_ACTION:
            if self.manifest.filter(uri):
                self.open_external(uri)
                decision.ignore()
            else:
                decision.use()
        else:
            print('POLICY:', uri, dtype)
            decision.use()


def run(manifest, basedir=None):
    dwas = DWAS(manifest, basedir)
    dwas.window.connect('destroy', Gtk.main_quit)

    Gtk.main()
