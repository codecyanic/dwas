# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
from .manifest import Manifest

__all__ = ['Manifest', 'run']


def app_name(manifest, basedir=None):
    if basedir:
        return os.path.abspath(basedir)

    return manifest.get('name', 'DWAS')


def run(manifest, basedir=None):
    sys.argv[0] = app_name(manifest, basedir)

    from .dwas import run
    run(manifest, basedir)
