# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
import yaml


class Manifest(dict):
    def __init__(self, manifest):
        super().__init__(yaml.safe_load(manifest))
        self._str = manifest

        if not self.get('allow'):
            self['allow'] = [self['url'] + '.*']

        if not self.get('deny'):
            self['deny'] = []

    def __str__(self):
        return self._str

    def filter(self, uri):
        """ Returns True if blocked """
        print_uri = uri.split(';')[0] if uri.startswith('data:') else uri
        allowed = False
        for m in self['allow']:
            if re.match(m + '$', uri):
                allowed = True
        if not allowed:
            print('Blocked (allow):', print_uri)
            return True

        for m in self['deny']:
            if re.match(m + '$', uri):
                print('Blocked (deny):', print_uri)
                return True

        print(print_uri)
        return False
