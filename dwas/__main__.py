# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import signal
import sys

from . import run, Manifest


def run_app(path):
    basedir = None
    try:
        with open(path) as f:
            m = f.read()
    except IsADirectoryError:
        basedir = path
        path = os.path.join(path, 'manifest.yaml')
        with open(path) as f:
            m = f.read()

    run(Manifest(m), basedir)


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    if len(sys.argv) <= 1:
        print('Usage:')
        print('  dwas app_directory')
        print('  dwas manifest')
        exit(2)

    path = sys.argv[1]
    run_app(path)


if __name__ == '__main__':
    main()
