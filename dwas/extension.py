# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from manifest import Manifest

import gi
gi.require_version('WebKit2WebExtension', '4.0')  # noqa E402
from gi.repository import WebKit2WebExtension  # noqa E401


class Extension:
    def __init__(self, extension, manifest):
        self.manifest = Manifest(manifest)
        extension.connect('page-created', self.on_page_created)

    def on_page_created(self, extension, webpage):
        webpage.connect('send-request', self.on_send_request)

    def on_send_request(self, webpage, request, data):
        uri = request.get_uri()
        return self.manifest.filter(uri)
