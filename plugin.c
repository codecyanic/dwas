/*
Copyright 2020 Cyanic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <pygobject.h>
#include <webkit2/webkit-web-extension.h>
#include <dlfcn.h>

typedef WebKitWebExtension WE;

static void update_import_path() {
    Dl_info dl_info;
    int got_info = dladdr(update_import_path, &dl_info);
    if (!got_info)
        return;

    char *module_path = strdup(dl_info.dli_fname);
    if (!module_path)
        return;

    PyObject *import_path = 0;
    char *end = strrchr(module_path, '/');
    if (end) {
        *end = 0;
        import_path = PyUnicode_FromString(module_path);
    }
    free(module_path);
    if (!import_path)
        return;

    PyObject *sys = PyImport_ImportModule("sys");
    if (sys) {
        PyObject *path = PyObject_GetAttrString(sys, "path");
        if (path) {
            PyList_Append(path, import_path);
            Py_DECREF(path);
        }
        Py_DECREF(sys);
    }
    Py_DECREF(import_path);
}

static PyObject *extension_class() {
    PyObject *class = 0;
    PyObject *module = PyImport_ImportModule("extension");
    if (module) {
        class = PyObject_GetAttrString(module, "Extension");
        Py_DECREF(module);
        if (class && !PyCallable_Check(class))
            Py_CLEAR(class);
    }
    return class;
}

static PyObject *extension_args(WE *ext, GVariant *data) {
    PyObject *tuple = 0;
    PyObject *pygobject = pygobject_init(-1, -1, -1);
    if (pygobject) {
        PyObject *webext = pygobject_new(G_OBJECT(ext));
        if (webext) {
            GValue value;
            g_dbus_gvariant_to_gvalue(data, &value);
            PyObject *args = pyg_value_as_pyobject(&value, FALSE);
            if (args) {
                tuple = Py_BuildValue("(OO)", webext, args);
                Py_DECREF(args);
            }
            Py_DECREF(webext);
        }
        Py_DECREF(pygobject);
    }
    return tuple;
}

G_MODULE_EXPORT
void webkit_web_extension_initialize_with_user_data(WE *ext, GVariant *data) {
    Py_Initialize();
    update_import_path();

    PyObject *class = extension_class();
    PyObject *args = extension_args(ext, data);

    if (class && args)
        Py_XDECREF(PyObject_CallObject(class, args));

    Py_XDECREF(args);
    Py_XDECREF(class);

    if (PyErr_Occurred())
        PyErr_Print();
}
