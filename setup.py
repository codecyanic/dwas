#!/usr/bin/env python3

# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess

from setuptools import setup, Extension


class PkgConfig():
    def __init__(self, *packages):
        self._packages = list(packages)

    def output(self, *args):
        cmd = ['pkg-config'] + list(args) + self._packages
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        return result.stdout.decode('utf-8').split()

    def cflags(self):
        return self.output('--cflags')

    def libs(self):
        return self.output('--libs')


packages = ['pygobject-3.0', 'webkit2gtk-web-extension-4.0', 'python3']
pkgconfig = PkgConfig(*packages)

setup(
    name='DWAS',
    version='0.5.1',
    packages=['dwas'],
    ext_modules=[Extension(
        'dwas/plugin',
        ['plugin.c'],
        extra_compile_args=pkgconfig.cflags(),
        extra_link_args=pkgconfig.libs()
    )],
    entry_points={
        'console_scripts': [
            'dwas = dwas.__main__:main',
        ],
    },
    zip_safe=False,
)
